//
// Created by PulsarV on 18-10-30.
//

#ifndef LIBSERV_BASE_MESSAGE_H
#define LIBSERV_BASE_MESSAGE_H

#include <queue>
#include<mutex>

namespace libserv {
    template<class T>
    class BaseMessage {
    public:
        BaseMessage(int max_queue_size) : max_queue_size(max_queue_size) {}

        T pop_message() {
            T message_front = message_queue.front();
            message_queue.pop();
            return message_front;
        }

        T front_message() {
            return message_queue.front();
        }

        T back_message() {
            return message_queue.back();
        }

        int size_message() {
            return message_queue.size();
        }

        void release_message() {
            while (message_queue.size() != 0)
                message_queue.pop();
        }

        void push_message(T prepare_message) {
            if (message_queue.size() < max_queue_size)
                message_queue.push(prepare_message);
        }

    public:
        std::queue<T> message_queue;
        static std::mutex message_mutex;
        int max_queue_size;
    };
}
#endif //LIBSERV_BASE_MESSAGE_H
