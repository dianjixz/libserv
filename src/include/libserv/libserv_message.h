#ifndef LIBSERV_MESSAGE_H
#define LIBSERV_MESSAGE_H

#include<libserv/libserv_base_message.hpp>
#include <map>
#include <functional>

namespace libserv {
    class Message : public BaseMessage<std::vector<char>> {
    public:
        Message(unsigned char head, int max_queue_size);

        void SerialRecive(char buffer);

        bool IsEmpty();

        void BindPackageCheck(bool(*check_function)(std::vector<char>));

        ~Message();

    private:
//        std::function<bool(std::vector<char>)> check_function;
        bool (*check_function)(std::vector<char>);

        unsigned char head;
        std::vector<char> prepare_message;
    };
}
#endif//LIBSERV_MESSAGE_H