//
// Created by wally on 2022/1/20.
//

#include <thread>
#include <cstring>
#include <ctime>
#include "libserv/libserv.h"
#include "JY901.h"

bool check_function(std::vector<char> package){
    return true;
}

//需要绑定的消息处理回调
int test_function(void *arg, std::vector<char> message) {
    if (message.size() < 11)return 0;
    char *temp_char = new char[message.size()];
    for (int i = 0; i < message.size() - 1; i++) {
        temp_char[i] = message[i];
    }
    struct STime		stcTime;//hijian
    struct SAcc 		stcAcc;//jiasudu
    struct SGyro 		stcGyro;//jiaosudu
    struct SAngle 		stcAngle;//jiaodu
    struct SMag 		stcMag;//cichang
    struct SDStatus 	stcDStatus;
    struct SPress 		stcPress;//qiya
    struct SLonLat 		stcLonLat;//jinweidu
    struct SGPSV 		stcGPSV;
    switch (message[1]) {
        case 0x50:	memcpy(&stcTime, &temp_char[2], 8);
            printf("Time:20%d-%d-%d %d:%d:%.3f\r\n", (short)stcTime.ucYear, (short)stcTime.ucMonth,
                   (short)stcTime.ucDay, (short)stcTime.ucHour, (short)stcTime.ucMinute, (float)stcTime.ucSecond + (float)stcTime.usMiliSecond / 1000);
            break;
        case 0x51:	memcpy(&stcAcc, &temp_char[2], 8);
            printf("Acc:%.3f %.3f %.3f\r\n", (float)stcAcc.a[0] / 32768 * 16, (float)stcAcc.a[1] / 32768 * 16, (float)stcAcc.a[2] / 32768 * 16);
            break;
        case 0x52:	memcpy(&stcGyro, &temp_char[2], 8);
            printf("Gyro:%.3f %.3f %.3f\r\n", (float)stcGyro.w[0] / 32768 * 2000, (float)stcGyro.w[1] / 32768 * 2000, (float)stcGyro.w[2] / 32768 * 2000);
            break;
        case 0x53:	memcpy(&stcAngle, &temp_char[2], 8);
            printf("Angle:%.3f %.3f %.3f\r\n", (float)stcAngle.Angle[0] / 32768 * 180, (float)stcAngle.Angle[1] / 32768 * 180, (float)stcAngle.Angle[2] / 32768 * 180);
            break;
        case 0x54:	memcpy(&stcMag, &temp_char[2], 8);
            printf("Mag:%d %d %d\r\n", stcMag.h[0], stcMag.h[1], stcMag.h[2]);
            break;
        case 0x55:	memcpy(&stcDStatus, &temp_char[2], 8);
            printf("DStatus:%d %d %d %d\r\n", stcDStatus.sDStatus[0], stcDStatus.sDStatus[1], stcDStatus.sDStatus[2], stcDStatus.sDStatus[3]);
            break;
        case 0x56:	memcpy(&stcPress, &temp_char[2], 8);
            printf("Pressure:%lx Height%.2f\r\n", stcPress.lPressure, (float)stcPress.lAltitude / 100);
            break;
        case 0x57:	memcpy(&stcLonLat, &temp_char[2], 8);
            printf("Longitude:%ldDeg%.5fm Lattitude:%ldDeg%.5fm\r\n", stcLonLat.lLon / 10000000, (double)(stcLonLat.lLon % 10000000) / 1e5, stcLonLat.lLat / 10000000, (double)(stcLonLat.lLat % 10000000) / 1e5);
            break;
        case 0x58:	memcpy(&stcGPSV, &temp_char[2], 8);
            printf("GPSHeight:%.1fm GPSYaw:%.1fDeg GPSV:%.3fkm/h\r\n\r\n", (float)stcGPSV.sGPSHeight / 10, (float)stcGPSV.sGPSYaw / 10, (float)stcGPSV.lGPSVelocity / 1000);
            break;
    }
    delete temp_char;
    return 0;
}

int main(int argv, char** argc)
{
    libserv::Serial serial("/dev/ttyUSB0", 9600,0x55);
    //绑定回调函数
    serial.bind_call(test_function);
    //绑定包校验函数
    serial.bind_package_check(check_function);
    //启动接收信息监听
    serial.Run();
    while (true) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));

        char buffer[5] = {(char)0xff, (char)0xaa, (char)0x03, (char)0x03, (char)0x00};
        //发送消息
//            serial.ansy_send(buffer, 5);
    }
    return 0;
}
